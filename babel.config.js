module.exports = function (api) {
    console.error('Inside babel config!');

    api.cache.never();

    return {
        presets: [
            '@babel/preset-env'
        ]
    }
}